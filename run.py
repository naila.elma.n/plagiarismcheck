from flask import Flask, render_template, redirect
from flask_wtf import FlaskForm
from wtforms import TextField, IntegerField, SubmitField
from stki_scripts.main import findSim
from stki_scripts.main import bow
from stki_scripts.main import findWord
from stki_scripts.main import findMirip
from stki_scripts.main import countMirip
from stki_scripts.main import hasilAkhir1
from stki_scripts.main import hasilAkhir2

app = Flask(__name__)
app.config.update(dict(SECRET_KEY='12345'))


class SearchTask(FlaskForm):
    keyword = TextField('Keyword')
    search = SubmitField('Search')

def searchTask(form):
    keyword = form.keyword.data
    path_corpus = "./text"
    res = findMirip(keyword, path_corpus)
    #res = {"title 1":0.3, "title 2":0.5, "title 3":1.3} # change the value here
    return res




@app.route('/', methods=['GET','POST'])
def main():
    # create form
    sform = SearchTask(prefix='sform')

    # get response
    data = {}
    hitung = {}
    akhir1 = 0
    akhir2 = 0
    #simpanBow = {} #bikin dictionary buat nampung hasil fungsi bow
    if sform.validate_on_submit() and sform.search.data:
        data = searchTask(sform)
        hitung = countMirip("./text/" + sform.keyword.data, './text')
        akhir1 = hasilAkhir1 ("./text/" + sform.keyword.data, './text')
        akhir2 = hasilAkhir2 ("./text/" + sform.keyword.data, './text')
    
    # render HTML
    return render_template('index.html', sform = sform, data = data,  hitung = hitung, akhir1 = akhir1, akhir2 = akhir2)

if __name__=='__main__':
    app.run(debug=True)