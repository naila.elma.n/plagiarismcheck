# -*- coding: utf-8 -*-
"""
Created on Wed Nov 15 23:25:03 2017

@author: Feedlyy
"""

import os

from difflib import SequenceMatcher

path = "./text"

listpath = {}
for item in os.listdir(path):
    if item.endswith(".txt"):
        with open(path + "/" + item, 'r') as file:
            listpath[item] = file.read()

path2 = "./text/bk.txt"
path3 = "./text/en.txt"


with open(path2) as file1, open(path3) as file2:
    path = file1.read()
    path = file2.read()
    similarity_ratio = SequenceMatcher(None, path, path).ratio()
    
    print similarity_ratio 
